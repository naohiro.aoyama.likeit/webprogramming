CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user (
    id SERIAL primary key AUTO_INCREMENT,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    is_admin boolean NOT NULL,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

INSERT INTO user VALUES(
    1,
    'admin',
    '�Ǘ���',
    '1995-04-22',
    'password',
    true,
    now(),
    now()
);

SELECT * FROM user WHERE login_id = 'admin' and password = 'password';

SELECT * FROM user WHERE login_id = 'admin' and password = 'hoge';

SELECT * FROM user;

INSERT INTO user(login_id,password,name,birth_date)VALUES(?,?,?,?);


USE usermanagement;

DROP TABLE user;

CREATE TABLE user (
    id SERIAL primary key AUTO_INCREMENT,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    is_admin boolean default 0 NOT NULL,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

INSERT INTO user VALUES(
    1,
    'admin',
    '�Ǘ���',
    '1995-04-22',
    'password',
    1,
    now(),
    now()
);

UPDATE user
SET password = 'ibaraki',
name = '��鑾�Y',
birth_date = '1990-07-10'
WHERE id = '2';
