package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/** Servlet implementation class LoginServlet */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/** @see HttpServlet#HttpServlet() */
	public UserAddServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("user-loginid");
		String password = request.getParameter("password");
		String passCon = request.getParameter("password-confirm");
		String userName = request.getParameter("user-name");
		String birthDay = request.getParameter("birth-date");

		UserDao userDao = new UserDao();
		userDao.userAdd(loginId, password, userName, birthDay);

		// || loginId == null || userName == null || birthDay == null
		if (!password.equals(passCon) || loginId == null || userName == null || birthDay == null) {

			// エラー文出してパスワード以外をセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDay", birthDay);

			// 新規登録画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// ユーザー一覧にリダイレクト
		response.sendRedirect("UserListServlet");

	}
}